/*TCap11_2.cpp.
NOTE TEORIA: 
1)la chiamata della funzione operatore pre incremento come funzione membro  è :  Oggetto& operator++() mentre globale sarà:
friend Oggetto& operator++(Oggetto &)
2) per l'operatore post fisso siccome a differenza di quello prefisso,esso viene usato per oggetti temporanei,non si ha una restituzione
di un riferimento. Inoltre per distinguerlo dalla funzione del punto 1) si crea un valore fittizio : Oggetto& operator++( int ) mentre globale
sarà: friend Oggetto& operator++(Oggetto &, int ) .*/
#include<iostream>
using namespace std;

class Date
{

	friend  ostream& operator<<(ostream& ,const Date &);
	public:
		Date(int m=1,int d =1,int y = 1900);
		void setDate(int,int,int);
		Date &operator++();//operatore postincremento
		Date  operator++(int i);
		const Date& operator+=(int);
		bool leapYear(int)const;//anno bisestile?
		bool endOfMonth(int)const;//ultimo giorno del mese?
	private:
		int month;
		int day;
		int year;
		
	static const int days[];
	void helpIncrement();

};

//membro static: una sola copia per tutti gli oggetti della classe
const int Date::days[]={ 0,31,28,31,30,31,30,31,31,30,31,30,31};

Date::Date(int m,int d,int y )
{
	setDate(m,d,y);
}
void Date::setDate(int mm,int dd,int yy)
{
	month = (mm>=1 && mm<=12)? mm: 1 ;
	year = (yy>=1900 && yy<=2100) ? yy:1900;
	
	if ( month == 2 && leapYear(year))
		day = (dd>=1 && dd<=29 ) ? dd:1;
	else
		day = (dd>=1 && dd<=days[month]) ? dd: 1;
		
}
Date& Date::operator++()
{
	helpIncrement();
	return *this;

}
Date Date::operator++(int)
{
	Date temp = *this;
	helpIncrement();
	return temp; //restituisce un oggetto temporaneo non incrementato
}
const Date& Date::operator+=(int additionalDays)//aumenta una data del numero specificato di giorni
{

	for(int i =0;i<additionalDays;i++)
		helpIncrement();

	return *this;//per operazioni in cascata
}
bool Date::leapYear(int testYear) const 
{

	if(testYear%400 ==0 || testYear%100!= 0 && testYear%4==0)
	
		return true;
	
	else 
		return false;//anno non bisestile

}
bool Date::endOfMonth(int testDay) const //determina se è l'ultimo giorno del mese
{

	if (month ==2&&leapYear(year))
		return testDay==29;
	else
		return testDay==days[month];
		

}
//funzione utilità per incremento della data
void Date::helpIncrement()
{
	if(!endOfMonth(day))//non è l'ultimo giorno del mese
		day++;
	else
		if(month<12) //ultimo giorno del mese e non è dicembre
		{
		
			month++;
			day=1;
		}else //ultimo giorno dell'anno
		{
			year++;
			month=1;
			day=1;
		
		}
}
ostream& operator<<(ostream & output,const Date&d)
{
	static char*monthName[13] = { ""," January","february","March","April","May","June","july","August","September","October","November","December"};
	output<<monthName[d.month] << ' ' <<d.day<< ", " <<d.year ;
	return output;
	


}
int main()
{
	Date d1;//Data di default , January 1,1900
	Date d3(2,27,1996);
		Date d2(2,02,8996);

	cout<<"d2 is " << d2 <<endl;
	cout<<"d1 is "<< d1 <<endl;
	cout<<"d3 is "<<d3 <<endl;
	
	cout<<" TEST OPERATORE PRE INCREMENTO :\n" 
		<<"d3 is "<<d3<<endl ;
		cout<< "++d3 is "<<++d3 <<endl;
		cout<<" d3 is " <<d3;

cout<<" TEST OPERATORE POST INCREMENTO :\n" 
		<<"d3 is "<<d3<<endl ;
		cout<< "d3++ is "<<d3++ <<endl;
		cout<<" d3 is " <<d3;
		
cout<<"Ridefinizione degli operatori post e pre incremento"<<endl;

return 0;
}