/*NOTE TEORIA: 
1) Gli operatori UNARI come "!" possono essere definite tramite funzione membro senza argomenti o tramite funzione globale(friend) con un argomento
che è un riferimento o una copia dell'oggetto in cui tale funzione è definita.  Nel primo caso avremmo qualcosa del tipo:
bool operator!() const ; nel secondo caso : friend bool operator!(const Oggetto &);//in questo caso c'è un riferimento
2) Gli operatori BINARI come" <" se definiti come funzioni membro allora il primo operando deve essere un obj delle classe . Ad esempio se 
y e z appartengo alla stessa classe si avrebbe : y.operator< (z) invocata dalla funzione membro : bool operator<( const Classe&)const;
Se definiti come friend o globale allora almeno un argomento deve essere della classe . E si avrebbe qualcosa del tipo : 
bool operator<(classe &,classe &);
*/
/*NOTA PROGRAMMA : progettazione di una classe array dove si ridefiniscono i seguenti operatori : << >> , ==, !=,[]*/

#include<iostream>
#include<iomanip>
#include<cstdlib>
using namespace std;

class Array {

	friend istream& operator>>(istream&, Array &);
	friend ostream& operator<<(ostream&, const Array &);
	
	public:
		Array(int =10);
		Array(const Array &);//costruttore di copia deve sempre avere un riferimento ad un oggetto costante per permette la copia di un oggetto costante
		~Array();
		int getSize() const;
		
		const Array& operator=(const Array & ) ; // ridefinizione operatore di assegnamento 
		bool operator==(const Array &) const ;//ridefinizione operatore di uguaglianza
		
		bool operator!=(const Array& right) const  //ridefinizione operatore di disuguaglianza
		{
			return !( *this == right); //invoca il risultato opposto della funzione sopra dell'operatore == 
		
		}
		int& operator[](int);//operatore di indicizzazione per ogetti non constanti
		int operator[](int) const; //operatore di indicizzazione per oggetti costanti. Non c'è un riferimento all'elemento perchè l'array essendo costante non può modificato
	private:
		int size;
		int*ptr;
		int num;

};
Array::Array( int arraySize)
{
	size = ( arraySize>0? arraySize : 10 );
	ptr = new int(size);
	num =10;
		for(int i =0;i<size;i++)
			ptr[i] =0;	

}
Array::Array(const Array &arrayToCopy):size(arrayToCopy.size) //costruttore di copia
{
	/*Ridefiniamo il costruttore di copia per evitare che l'array uguagliato all'array di input dipenda da quest'ultimo
	. Senza il costruttore di copia infatti si condividerebbe la stessa area di memoria. Si usa il riferimento dell'oggetto in input
	per evitare che venga richiamata il costruttore di copia,cioè la funzione che stiamo ridefinendo,portando così ad una ricorsione infinita.*/
	
	ptr = new int[size]; //fondamentale viene creato uno spazio di memoria per l'array
	for (int i =0;i<size;i++)
		ptr[i] = arrayToCopy.ptr[i];
}
Array::~Array()
{

	delete [] ptr;
}
int Array::getSize() const 
{

	return size;
}
//ridefinizione operatore di assegnamento La restituzione di un oggetto const impedisce cose del tipo (a1=a2)=a3.
const Array& Array::operator=(const Array &right)
{
	if (&right!= this) //evita autoassegnamento
	{
	
		if (size!=right.size)//? 
		{
			//per array di dimensioni diversi dealloca lo spazio dell'array di sinistra che dovrà essere assegnato a quello di destra
		delete []ptr;
		size = right.size;
		ptr = new int [size];
		
		}
		
		for (int i =0; i<size;i++)
			ptr[i] = right.ptr[i];
			
		}
	return *this; //per assegnamenti in cascata come x=y=z  NOTA: ( *this).metodo .
		
}
bool Array::operator==(const Array &right) const
{
	if (right.size!=size) return false;//array dimensioni diverse
	
	for(int i=0;i<size;i++)//
		if(ptr[i]!=right.ptr[i]) return false;
	
	return true;		

}
istream& operator>>(istream&input,Array & a)
{
	for(int i=0;i<a.size;i++)
		input>>a.ptr[i];

return input;

}
ostream& operator <<(ostream&output,const Array &a)
{
	for(int i=0;i<a.size;i++)
		output<<a.ptr[i]<<endl;
	
	return output;


}
//ridefinizione dell'operatore di indicizzazione per array non costanti
int& Array::operator[](int subscript) 
{

	if (subscript<0 || subscript>=size) 
	{cout<<"Errore" <<"out of range"<<endl; 
		exit(1);}
	
	return ptr[subscript]; //restituisce un riferimento all'elemento contenuto in ptr[subscript]

}
//ridefinizione dell'operatore di indicizzazione per array  costanti
int Array::operator[](int subscript) const 
{
	if (subscript<0 || subscript>=size) 
		{cout<<"Errore" <<"out of range"<<endl; exit(1);}


	return ptr[subscript];//restituisce una copia e non un riferimento. Questo perchè l'array è costante e il suo contenuto cioè i suoi elementi non possono essere modificati

}
int main()
{
cout<<"Ridefinizione degli operatori"<<endl;

Array arr1(3);
Array arr2;
Array arr3(3);


//visualizza dimensione degli array

cout<<"Size of Array arr1 is " <<arr1.getSize()<<endl;
cout<<"Size of Array arr2 is " <<arr2.getSize()<<endl;

cout<<"Riempi Array arr1"<<endl;
cin>>arr1;
//visualizza array 1 
cout<<"Print Array arr1 : "<<endl<<arr1<<endl;
cout<<"Riempi Array arr3"<<endl;
cin>>arr3;

if (arr1 ==arr3) cout<<"Sono uguali "<<endl;
if (arr1 ==arr2) cout<<"Sono uguali "<<endl;
	else 
		cout<<"Sono diversi" <<endl;
arr2 = arr1; //NON invoca costruttore di copia perchè è un assegnamento non inizializzazione
Array arr4 = arr3 ; //invoca costruttore di copia
Array arr5(arr1) ; //invoca costruttore di copia
if (arr1 ==arr2) cout<<"Sono uguali "<<endl;
	else 
		cout<<"Sono diversi" <<endl;

cout<<"elemento alla posizione 0  = " << arr1[0]<<endl;
arr1[ 0] = 69;
cout<<"elemento alla posizione 0  = " << arr1[0]<<endl;

//viene chiamato la funzione operator[]( int ) e nel caso suddetto : arr1.operator[ ]( 0 ) 
return 0;
}

