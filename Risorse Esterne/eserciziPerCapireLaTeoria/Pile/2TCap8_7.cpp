#include <iostream>//STACK DINAMICA MEDIANTE DELEGAZIONE della classe lista
using namespace std;

class Node
{
	public:
		Node(const int& input):data(input),nextPtr(0),prevPtr(0){}
		
		int getData() const{return data;}
		void setData(const int& input) { data = input;}
		
		Node* getNextPtr() const {return nextPtr;}
		void setNextPtr( Node*next) { nextPtr = next;}
		
		Node* getPrevPtr() const {return prevPtr;}
		void setPrevPtr( Node*prev) { prevPtr = prev;}
		
	private:
		int data;
		Node*nextPtr;
		Node*prevPtr;
		
};
class List
{
	public:
		List():head(0),tail(0){};
		~List();
		List* insertAtFront(const int&);
		List* insertAtBack(const int&);
		List* insertInOrder(const int&);
		List* removeFromFront();
		List* removeFromBack();
		List* removeData(const  int&);
		void print() const ;
	private:
		Node*head;
		Node*tail;
		bool isEmpty() const { return head==0;}
		Node* getNewNodo(const int&input) {return new Node(input);}
		
};
List::~List()
{

	Node*alias = 0;
	while ( head!=NULL)
	{
		alias = head;
		head = head->getNextPtr();
		delete alias ;
		alias =0;
	}
	head =0;
}
void List::print() const 
{
	cout<<"PRINT"<<endl;
	if (isEmpty()==true)
		cout<<"List is empty. So it can't be possibile print()" <<endl;
	else
		{
			Node*indice =0;
			for (indice = head; indice!=NULL;indice= indice->getNextPtr() ) 
					cout<<"Data is : " <<indice->getData()<<endl;		
		}
		cout<<"---"<<endl;
}

List* List::insertAtFront(const int & input)
{
	Node*newData = getNewNodo(input);
	if ( isEmpty()==true)
		head=tail = newData;
	else
		{
			newData->setNextPtr(head);
			head->setPrevPtr(newData);
			newData->setPrevPtr(0);
			head = newData;
		}
	return this;
}
List* List::insertAtBack(const int& input)
{
	Node*newData = getNewNodo(input);
	if ( isEmpty() == true)
		List::insertAtFront(input);
	else 
		{
			tail->setNextPtr(newData);
			newData->setPrevPtr(tail);
			tail = newData;
		}
	return this;
}
List* List::insertInOrder(const int& input)
{
	Node*newData = getNewNodo(input);
	if ( isEmpty() == true)
		List::insertAtFront(input);
	else if ( head->getData()>input)
			List::insertAtFront(input);
		else 
			{
				Node*current = head;
				while ( current!=NULL && current->getData()<input)
				current= current->getNextPtr();
			
				if (current==NULL)
					List::insertAtBack(input);
				else 
					{	
						newData->setPrevPtr( current->getPrevPtr() );
						current->setPrevPtr(newData);
						newData->setNextPtr(current);
				
					}
			}

	return this;
}
List* List::removeData(const int& input)
{
	if (isEmpty()==true)
				cout<<"List is empty. It can't be possibile removeData "<<endl;
	else if ( head->getData()==input)
			List::removeFromFront();
		else if (tail->getData()==input)
				List::removeFromBack();
			else  //il dato è in mezzo
				{
					Node*current = head->getNextPtr();
					
					while(current!=NULL && current->getData()!=input)
						current = current->getNextPtr();
					if (current==NULL)
						cout<<" Data is not in your list"<<endl;
					else
						{
							current->getPrevPtr()->setNextPtr( current->getNextPtr() ) ;
							current->setPrevPtr( current->getPrevPtr() ) ;
							current->setPrevPtr( 0);
							current->setNextPtr(0);
							delete current;
							current =0;
							
						}			
				
				}


	return this;
}
List* List::removeFromFront()
{
	if (isEmpty() == true)
		cout<<"List is empty. It can't be possibile removeFromFront() "<<endl;
	else if (head==tail) //list ha un solo elemento
		{
			delete head ;
			head=tail=0;
		
		}
	else 
		{
			Node*alias = head;
			head = head->getNextPtr();
			alias->setNextPtr(0);
			alias->setPrevPtr(0);
			delete alias;
			alias =0; 
		
		}
	return this;
}
List* List::removeFromBack()
{
	if (isEmpty() == true)
		cout<<"List is empty. It can't be possibile removeFromFront() "<<endl;
	else if (head==tail) //list ha un solo elemento
		List::removeFromFront();
	else 
	{	
		Node*alias = tail->getPrevPtr();
		alias->setNextPtr(0);
		tail->setPrevPtr(0);
		tail->setNextPtr(0);
		delete tail;
		tail =0;
		tail = alias;	
	
	}
	return this;
}
//-------------------classe Pila per Delegation-----------
class Stack : private List
{
	public:
		Stack& push(const int &input ) { insertAtFront(input); return *this; }
		Stack& pop() { removeFromFront() ; return *this;}
		void printStack() const { print() ; }
				
};
int main()
{
	Stack stack0;
	stack0.printStack();
	stack0.push(1).push(2).push(3).push(4).printStack();
	/*stack0.pop().pop().pop().pop().pop();
	stack0.print();
	stack0.pop();*/

return 0;
}