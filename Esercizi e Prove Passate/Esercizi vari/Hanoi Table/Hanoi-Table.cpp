#include <iostream>
using namespace std;

template <class H> class ArrayStack{
	H* q;
	int top, length, current;

public:
	ArrayStack(){
		length = 5;
		q = new H[length];
		top = 0;
	}

	int isEmpty(){ return top == 0; }
	int isFull(){ return top == length; }
	int getSize(){ return top; }

	void push(H x){
		if(!isFull())
			q[top++] = x;
	}

	H* pop(){
		if(!isEmpty())
			return &q[--top];
		return NULL;
	}

	H* begin(){
		if(!isEmpty()){
			current = top-1;
			return &q[current];
		}
		return NULL;
	}

	H* next(){
		if(current-1 >= 0) return &q[--current];
		return NULL;
	}

	H* topStack(){
		if(!isEmpty())
			return &q[top-1];
		return NULL;
	}
};

template <class H> ostream& operator<<(ostream& out, ArrayStack<H>& obj){
	for(H* it = obj.begin(); it; it = obj.next())
		out << *it << " ";
	return out;
}

template <class H> class HTable { 
public: 
	virtual HTable<H>* push(H x, int p) = 0; 
	virtual H* pop(int p) = 0; 
	virtual HTable<H>* move(int p, int q) = 0; 
	virtual void print() = 0; 
};

template <class H> class MyHTable: public HTable<H>{
	ArrayStack<H>* table;
	int length, n;

public:
	MyHTable(int length){
		this->length = length;
		table = new ArrayStack<H>[length];
	}

	HTable<H>* push(H x, int p){
		if(table[p].topStack() && *table[p].topStack() < x){
			cout << "Impossibile inserire l'elemento " << x << " nella pila " << p << endl;
			return this;
		}else if(n+1 == length * 2){
			cout << "Impossibile inserire ulteriori elementi nella struttura!" << endl;
			return this;
		}

		if(!table[p].isFull()){
			table[p].push(x);
			n++;
		}

		return this;
	}

	H* pop(int p){
		if(!table[p].isEmpty()){
			n--;
			return table[p].pop();
		}
		return NULL;
	}

	HTable<H>* move(int p, int q){
		if(!table[p].isEmpty()){
			H* tmp = table[p].pop();
			if(!table[q].isFull()){
				table[q].push(*tmp);
			}
		}
		return this;
	}

	void print(){
		for(int x = 0; x < length; x++)
			cout << table[x] << endl;
		cout << endl;
	}
};


int main(){
	MyHTable<int>* HT = new MyHTable<int>(3);
	HT->push(10, 0)->push(5, 0);
	HT->push(32, 1)->push(21, 1)->push(8, 1);
	HT->push(17, 2)->push(4, 2)->push(3, 2);
	HT->print();

	int* tmp = HT->pop(2);
	HT->push(*tmp, 0);

	HT->move(1, 2)->print();
}

