Stefano Borzì {
  - Cancellazione nodo in una struttura dati binaria
  - Dividi et impera
  - MergeSort e complessità
}



Alfio Palermo { -Pila e Coda  implementazioni sia come liste sia come array }



Trupia Ludovico {
  - Differenza della Complessità tra gli algoritmi Sort:Selection, Insertion, Merge e Quick
  - Numero di chiamate ricorsive nel QuickSort
  - MergeSort e complessità totale dell'algoritmo e della parte di Fusione(Merge)
}




Tascone Danilo {
  - Complessità spaziale del quicksort nel caso peggiore? Ovvero quante chiamate ricorsive sono attive contemporaneamente? Scrivere l'albero di ricorsione.
  - Allocazione dinamica e statica, heap e stack.
  - Come implementare una coda utilizzando due Stack.
  - Dato un array di n-elementi e volendo implementare una struttura dati dinamica, cosa faccio quando termino lo spazio nell'array? Risposta: Si raddoppia la
  - dimensione. 
}



Elia Vacanti {
  - implementazione stack e coda
  - overloading overriding
  - variabili globali
}




Alberto Costa {
   - Albero (in generale, definizione...ecc.)
   - merge sort
   - divide et impera
   - definizione variabile static
}


Adriano Ferraguto { Merge Sort - Visibilità di variabili - Alberi (definizione di albero, altezza di un albero binario ben bilanciato, altezza massima di un albero) }




Alessio Piazza {
	- Complessità temporale e spaziale nei casi medio e pessimo di selection, insertion, quicksort e mergesort
	- Come si comportano nel caso di un array già ordinato
	- Albero di ricorsione del caso pessimo del quicksort
	- Fuzioni const
}

Altre Domande {
   - Complessità di tutte le operazioni effettuabili su alberi
   - Trovare il successivo di un elemento in un albero
}



Vinz Filetti {
	- Procedura ottimizzata per l'inserimento dell'n+1 elemento in un array di dimensione n, pieno.
	- Procedura ottimizzata per la cancellazione di un elemento da un array di dimensione n.
	- Ricerca in un array ordinato, array disordinato, lista ordinata.
	- Parametri di Default.
	- Complessità ricerca in un albero.
	- Complessità ricerca minimo in un albero.
	- Numero massimo di nodi in un albero di n livelli.
}



Erika Molino {
	- Definizione ricorsiva di albero e lista
	- Complessità algoritmi di ordinamento
	- Implementazione partition
	- Passaggio parametri nelle funzioni
	- Static factory
}



Mauro Mazza {
	- Definizione e implementazione (non ho scritto il codice, sia chiaro) di un array circolare;
	- Complessità dell'unione di due array già ordinati in un unico array ordinato;
	- Algoritmo da preferire nella procedura d'ordinamento sopra citata , motivo e se presenti vantaggi in termini di complessità;
	- Ho parlato a raffica e nel farlo ho esposto più algoritmi e quindi ho dovuto dirgli le varie complessità : Selection sort -> Qiuick Sort -> Merge Sort;
}


Marco Costanzo {
	- Algoritmi di ordinamento
	- BubbleSort ottimizzato e non ottimizzato con rispettive complessità temporali ed esempio nel caso peggiore
	- ricerca in un array ordinato (codice)
	- ricerca dicotomica
}



Salvo Borgesi {
	- Definizione variabile REGISTER 
	- complessità Quicksort
	- funzionamento Quicksort
}



Vincenzo Aliperti {
	- Cos'è il predecessore in un BST (hanno voluto la risposta secondo le dispense di analisi/discreta, quella ordinaria secondo i BST non basta, questo almeno per catalano)
	- Definizione ricorsiva di Lista
}



Alessio Liotta { // cit. "Ma chiddi su cuntaturi!!"
	- Complessità di un ciclo FOR con incrementi differenti
	- Differenza static e const
	- Visibilità di variabili con lo stesso nome ma diverso tipo
	- Definizionericorsiva di albero.
}



Gabriele Cubeda {
	- Static Factory
	- Funzione statica
	- for logaritmico ( semplicemente -> for(int i = 1; i < n; i = i * 2); )
	- Operatore di scope ( per variabile globale-> ::nomeVariabile )
	- Binding
}
	

Giorgio Balsamo { 
	- Cancellazione e inserimento in un array con complessità
	- Implementazione ricerca binaria iterativa 
	- metodo per vedere se un array è ordinato
}



Alessandro Catalano {
    - Natural Fill
    - BFS (funzionamento, codice e complessità)
    - dato un array NON ORDINATO: complessità della rimozione di un elemento e complessità se volessimo riempire questo buco
}


Pierpaolo Pecoraio {
    - Costruttore di Copia
    - Static Factory
    - Scrivere funzione che calcoli logaritmo (sia iterativo che ricorsivo)
}
