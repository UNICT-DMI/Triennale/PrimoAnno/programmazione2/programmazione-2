#include <iostream>
using namespace std;
template <class T>
struct nodo{
	T  *A;
	int i;
	nodo<T>*next;
	nodo<T>*prec;
};

template <class H> class ArrayList {
public:
	virtual ArrayList<H>* ins(H x) = 0;
	virtual ArrayList<H>* del(H x) = 0;
	virtual int search(H x) = 0;
	virtual void print() = 0;
};


template <class T>
class MyArrayList : public ArrayList < T > {

public:
	int k;
	nodo<T>*inizio;
	MyArrayList(){}
	MyArrayList(int x) {
		this->inizio = NULL;
		k = x;
	}
	~MyArrayList(){
	}
	MyArrayList<T>* ins(T x) {
		if (this->inizio == NULL){

			this->inizio = new nodo < T > ;
			this->inizio->A = new T[k];
			this->inizio->next = NULL;
			this->inizio->prec = NULL;
			this->inizio->i = 0;
			this->inizio->A[inizio->i] = x;
		}
		else{
			nodo<T> *app = this->inizio;
			while (app->next != NULL){
				app = app->next;
			}
			if (app->i == k - 1) {
				nodo <T>* nuovo = new nodo < T > ;
				nuovo->A = new T[k];
				nuovo->next = NULL;
				nuovo->prec = app;
				nuovo->prec->next = nuovo;
				nuovo->i = 0;
				nuovo->A[nuovo->i] = x;
			}
			else{
				app->i += 1;
				app->A[app->i] = x;
			}
		}
		return this;
	}


	void print(){
		nodo<T>*app = this->inizio;
		int j = 0;
		while (app != NULL){
			for (j = 0; j<app->i + 1; j++){
				cout << "	" << app->A[j];
			}
			cout <<endl<< "|||||||||||||||||||||||||||||||||||||||||||" << endl;
			app = app->next;
		}

	}

	int search(T x) {
		nodo<T>*app = this->inizio;
		while (app != NULL){

			for (int j = 0; j<app->i+1; j++){
				if (app->A[j] == x)
					return 1;
			}
			app = app->next;
		}
		return 0;
	};

	MyArrayList<T>* del(T x) {
		nodo<T>*app = this->inizio;
		bool found = false;
		int pos = 0;
		if (this->search(x) == 1){
			while (app != NULL && !found){               //trovo in che nodo e in che posizione � locato l'elemento da eliminare
				for (int j = 0; j < app->i + 1; j++){
					if (app->A[j] == x){
						pos = j;
						found = true;
					}
				}
				if (!found)
					app = app->next;
			}
				if (pos == app->i){
					if (app->next == NULL){
						app->i = app->i - 1;
						if (app->i < 0) { app->prec->next = NULL; delete app; }
						return this;
					}
					else{
						app->A[app->i] = app->next->A[0];
					}
				}
				else{
					for (int j = pos; j < app->i; j++){
						app->A[j] = app->A[j + 1];
					}
					if (app->next != NULL) 	app->A[app->i] = app->next->A[0]; 
				}
				if (app->next == NULL){
					app->i = app->i - 1;
					if (app->i < 0) { app->prec->next = NULL; delete app; }
					return this;
				}
				else{
					app = app->next;
					while (app != NULL){
						for (int j = 0; j <app->i; j++){
							app->A[j] = app->A[j + 1];
						}
						if (app->next != NULL)app->A[app->i] = app->next->A[0]; else{ app->i -= 1; if (app->i < 0) { app->prec->next = NULL;  } }
						app = app->next;
					}
				}
		}
		else{
			cout << "elemento non presente" << endl;
		}
		return this;
	};
};

template <class T> 
class OrderedArrayList: public MyArrayList<T>{
public:
	OrderedArrayList(int x){
		this->inizio = NULL;
		this->k = x;
	}
	~OrderedArrayList(){}
	OrderedArrayList<T>* ins(T x) {
		if (this->inizio == NULL){
			this->inizio = new nodo < T >;
			this->inizio->A = new T[this->k];
			this->inizio->next = NULL;
			this->inizio->prec = NULL;
			this->inizio->i = 0;
			this->inizio->A[this->inizio->i] = x;
		}
		else{
			nodo<T>*app = this->inizio;
			bool done = false;
			while (!done){
				int j = 0;
				while (j<=app->i){
					if (app->A[j] > x){ T temp; temp = app->A[j]; app->A[j] = x; x = temp; }
					j++;
				}if (app->i < this->k - 1){
					app->i=app->i+1;
					app->A[app->i] = x;
					done = true;
				}
				else{
					if (app->next == NULL){
						nodo<T>*nuovo = new nodo<T>;
						nuovo->A = new T[this->k];
						nuovo->i = 0;
						nuovo->next = NULL;
						nuovo->prec = app;
						app->next = nuovo;
						nuovo->A[0] = x;
						done = true;
					}
					else{
						app = app->next;
					}
				}
			}
		}
		return this;
	}
	int rank(T x){
		if (this->search(x) == 1){
			int cont=0;
			bool done = false;
			nodo<T>*app = this->inizio;
			while (app != NULL && !done){
				for (int j = 0; j < app->i + 1 && !done; j++, cont++)
					if (app->A[j] == x)done = true;
				app = app->next;
			}
			return cont;
		}
		else{
			cout << "Elemento non presente" << endl;
			return 0;
		}
	}
};


int main(){
	int dainserire[] = { 23, 4, 6, 8, 12, 21, 5, 9, 7, 3, 16, 2, 24 };
	int dacancellare[] = { 4, 5, 16, 2 };
	int orank[] = { 3, 8, 23 };
	int dim = 5;
	MyArrayList<int>*mio = new MyArrayList<int>(dim);
	OrderedArrayList<int>*ormio = new OrderedArrayList<int>(dim);
	cout << "Inserimento valori MyArrayList :" << endl;
	for (int i = 0; i<13; i++){
		mio->ins(dainserire[i]);
	}
	cout << endl;
	mio->print();
	cout << endl;
	
	for (int i = 0; i < 4; i++){
		mio->del(dacancellare[i]);
	}
	cout << "Dopo del MyArrayList" << endl;
	mio->print();
	cout << endl;
	cout << "Inserimento valori OrderedArrayList :" << endl;
	for (int i = 0; i<13; i++){
		ormio->ins(dainserire[i]);
	}
	cout << endl;
	ormio->print();
	cout << endl;
	for (int i = 0; i < 4; i++){
		ormio->del(dacancellare[i]);
	}
	cout << "Dopo del OrderedArrayList" << endl;
	ormio->print();
	cout << endl;
	cout << "Funzione Rank : " << endl;
	for (int i = 0; i < 3; i++){
		cout << orank[i] << "\tRank\t" << ormio->rank(orank[i]) << endl;
	}
	getchar();
	return 7;
}
