#include <iostream>
using namespace std;

template <class A> class Node{
	private:
			A key; int mul; 
			Node <A>* next;
	public:
			Node(A key) {
				this->key = key;
				next  = NULL;
			}
			
			Node() {
				next  = NULL;
			}
			
			void setNext(Node<A>* next){ this->next = next; }
			void setKey(A key){ this->key = key; }
			
			Node <A>* getNext(){ return this->next; }

			A getKey() { return this->key; }
};

template <class H> class List {
	public:/*
	virtual List<H>* ins(H *x) = 0;
	virtual List<H>* del(H *x) = 0;
	virtual int search(H *x) = 0;
	virtual void print() = 0;
	*/
};

template <class H> class MyList: public List<H> {
	private:
			Node <H>* header, * trailer;
			int size;
	public:
			MyList() {
				header = new Node<H>();
				trailer = new Node<H>();
				header->setNext(trailer),
				size = 0;
			}
			
			List<H>* _ins(H x) {
				Node <H>* newNode = new Node<H>(x);
				newNode->setNext(header->getNext());
				header->setNext(newNode);
				size++;
				return this;
			}
			List<H>* ins(H*x) {
				return _ins(*x);
			}
			
			Node <H>* _search(H x){
				if(!size) return NULL;
				Node <H>* temp = header->getNext();
				while(temp != trailer) {
					if(temp->getKey() == x) return temp;
					temp = temp->getNext();
				}
				return NULL;
			}
			
			int search(H *x){
				return _search(*x)? 1 : 0;
			}
			
			List<H>* _del(H x) {
				if(!size) return this;
				Node <H>* temp = _search(x);
				if(!temp) return this;
				Node <H>* WTemp = header->getNext();
				Node <H>* prev = header;
				while(WTemp != temp) {
					prev = WTemp;
					WTemp = WTemp->getNext();
				}
				prev->setNext(temp->getNext());
				size--;
				return this;
			}
			
			List<H>* del(H*x) {
				return _del(*x);
			}
			
			void print() {
				Node <H>* temp = header->getNext();
				while(temp != trailer){
					cout << temp->getKey() << " ";
					temp = temp->getNext();
				}
				cout << endl;
			}
			
						
			
			
};

template <class P> class OrderedList : public MyList <P>{
	private:
			Node <P>* header, * trailer;
			int size;
	public:
			OrderedList(){
				header = new Node<P>();
				trailer = new Node<P>();
				header->setNext(trailer);
				size = 0;
			}
			
			List<P>* _ins(P x){
				Node <P>* newNode = new Node<P>(x);
				Node <P>* temp = header;
				while(temp->getNext() != trailer) {
					if(temp->getNext()->getKey() < x) break;
					temp = temp->getNext();
				}
				newNode->setNext(temp->getNext());
				temp->setNext(newNode);
				size++;
				return this;
			}
			
			
			void print2(){
				if(!size) return;
				Node <P>* temp = trailer;
				Node <P>* temp2 = header;
				while(temp != header) {
					while(temp2->getNext() != temp) {
						temp2 = temp2->getNext();
					}
					cout << temp2->getKey() << " ";
					temp = temp2;
					temp2= header;
				}
				cout << endl;
			}
};

template <class I> class Stack : public MyList<I>{
	public:
		virtual void push(I *x) = 0;
		virtual I pop() = 0;
};

template <class E> class MyStack : public Stack <E> {
	private:
			Node <E>* header, * trailer;
			int size;
	public:
			MyStack() {
				header = new Node<E>();
				trailer = new Node <E>();
				header->setNext(trailer);
				size = 0;
			}
			void _push(E x) {
				this->_ins(x);
			}
			
			void push(E *x) {
				_push(*x);
			}
			E pop() {
				if(size) {
					E val = header->getNext()->getKey();
					header->setNext(header->getNext()->getNext());
					size--;
					return val;
				}
				return -1;
			}
};


 



int main(){
	int MyArray[] = {23 ,4 ,6, 8 ,12 ,21 ,5 ,9 ,7 ,3 ,16 ,2 ,24};
	/*
	MyList<int>* Listoccia = new MyList<int>();
	for(int i = 0; i< 13;i++) Listoccia->ins(&MyArray[i]);
	Listoccia->print();
	// 4,5,16,2
	Listoccia->del(&MyArray[1]);
	Listoccia->del(&MyArray[6]);
	Listoccia->del(&MyArray[10]);
	Listoccia->del(&MyArray[11]);
	//23 6 8 12 21 9 7 3 24
	Listoccia->print();
	*/
	/*
	OrderedList<int>* Listella = new OrderedList<int>();
	//
	for(int i = 0; i< 13;i++) Listella->ins(&MyArray[i]);
	Listella->print();
	Listella->del(&MyArray[1]);
	Listella->del(&MyArray[6]);
	Listella->del(&MyArray[10]);
	Listella->del(&MyArray[11]);
	Listella->print();
	// 3 6 7 8 9 12 21 23 24
	*/
	MyStack<int>* Stacketto = new MyStack<int>(); 
	for(int i = 0; i< 13;i++) Stacketto->push(&MyArray[i]);
	Stacketto->print();
	Stacketto->del(&MyArray[1]);
	Stacketto->del(&MyArray[6]);
	Stacketto->del(&MyArray[10]);
	Stacketto->del(&MyArray[11]);
	Stacketto->print();
}
