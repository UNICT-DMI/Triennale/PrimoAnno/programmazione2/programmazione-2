#include <iostream>
using namespace std;

template <class A> class Node{
	private:
			A key; 
			Node <A>* next, *prev;
	public:
			Node(A key) {
				this->key = key;
				next = prev = NULL;
			}
			
			Node() {
				next = prev = NULL;
			}
			
			void setNext(Node<A>* next){ this->next = next; }
			void setPrev(Node<A>* prev){ this->prev = prev; }
			void setKey(A key){ this->key = key; }
			
			Node <A>* getNext(){ return this->next; }
			Node <A>* getPrev(){ return this->prev; }
			A getKey() { return this->key; }
};

template <class P> class SDList {
	public:
	virtual SDList<P>* ins(P x) = 0;
	virtual void print() = 0;
};

template <class H> class MySDList: public SDList <H> {
	private:
			Node <H>* trailer, *header;
			int size;
			Node <H>* shortuctsArray[100];
	public:
			MySDList(){
				trailer = new Node<H>();
				header = new Node<H>();
				header->setNext(trailer);
				trailer->setPrev(header);
				size = 0;
				for(int i = 0; i < 100;i++)	shortuctsArray[i] = NULL; 
			}
			
			MySDList<H>* ins(H x){
				Node<H>* newNode = new Node<H>(x);
				Node<H>* temp = header->getNext();
				while(temp != trailer) {
					if(temp->getKey() > x) break;
					temp = temp->getNext();
				}
				temp->getPrev()->setNext(newNode);
				newNode->setNext(temp);
				newNode->setPrev(temp->getPrev());
				temp->setPrev(newNode);
				size++;
				return this;
			}
			
			void print() {
					if(!size) return;
					Node<H>* temp = header->getNext();
					while(temp != trailer) {
						cout << temp->getKey() << " "; 
						temp = temp->getNext();
					}
					cout << endl;
			}
			
			Node <H>* _search(H x) {
				if(!size) return NULL;
				Node <H>* temp = header->getNext();
				while(temp != trailer) {
					if(temp->getKey() == x) return temp;
					temp = temp->getNext();
				}
				return NULL;
			}
			
			int search(H x) {
				return _search(x)? 1:0;
			}
			MySDList<H>* del(H x) {
				Node <H>* temp = _search(x);
				if(!temp) return this;
				temp->getPrev()->setNext(temp->getNext());
				temp->getNext()->setPrev(temp->getPrev());
				return this;	
			}
			
			void ArrangeArray(){
				if(!size){ 
					for(int i = 0; i < 100;i++) shortuctsArray[i] = NULL;
				}
				Node<H>* temp = header->getNext();
				
				for(int i = 0; i < size;i++){
					while(temp != trailer){
						if(temp->getKey() > 10*i) {
							shortuctsArray[i] = temp;
							break;
						}
						else shortuctsArray[i] = NULL;
						temp = temp->getNext(); 
					}
					temp = header->getNext();
				}
			}
			
			void directPrint(){
				ArrangeArray();
				for(int i = 0; i < size;i++){
					if(shortuctsArray[i] != NULL) {
						cout << "i*10 = " << i*10 << "   ";
						cout << "Shortcut: "<<shortuctsArray[i]->getKey();
						cout << endl;
					}
				}
			}
};

int main(){
	MySDList<int>* ListaDiretta = new MySDList<int>();
	int MyArray[] = {7,13,2,6,9,10,21,32,4,12};
	for(int i = 0; i < 10; i++) ListaDiretta->ins(MyArray[i]);
	ListaDiretta->print();
//	ListaDiretta->del(7)->del(4)->del(32)->del(10)->print();	
	ListaDiretta->directPrint();
	
}




