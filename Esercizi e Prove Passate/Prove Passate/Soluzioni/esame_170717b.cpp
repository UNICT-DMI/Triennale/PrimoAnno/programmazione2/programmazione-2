//i valori di test del main non sono quelli forniti nella traccia del compito

#include <iostream>
#define DIM 20

using namespace std;

class SDCode{

	private:
		int fir;
		const char* cab;

	public:
		SDCode(int f, const char *c) : fir(f), cab(c) {}

		void setFir(int f){
			fir=f;
		}

		void setCab(const char* c){
			cab=c;
		}

		int getFir() const{
			return fir;
		}

		const char* getCab() const{
			return cab;
		}

		friend bool operator < (const SDCode &x, const SDCode &y){
			if(x.getFir() < y.getFir()){
				return true;
			}

			bool flag=false;
			for(int i=0; i<10; i++){
				int a=x.getCab()[i];
				int b=y.getCab()[i];
				if(a < b){
					flag=true;
					break;
				}
				else if(a>b){
					break;
				}
			}

			if(x.getFir() == y.getFir() && flag==true){
				return true;
			}
			return false;
		}

		friend bool operator > (const SDCode &x, const SDCode &y){
			return y<x;
		}

		friend bool operator == (const SDCode &x, const SDCode &y){
			return !(x<y) && !(x>y);
		}

		friend bool operator != (const SDCode &x, const SDCode &y){
			return !(x==y);
		}

		friend bool operator <= (const SDCode &x, const SDCode &y){
			return !(x>y);
		}

		friend bool operator >= (const SDCode &x, const SDCode &y){
			return !(x<y);
		}

		friend ostream& operator << (ostream &stream, const SDCode &x){
			return stream<<"("<<x.getFir()<<", "<<x.getCab()<<")";
		}
};


template <class H> class Stack{
	
	protected:
		H** v;
		int n;
		int top;

		bool _isFull(){
			return n==DIM ? true : false;
		}

		bool _isEmpty(){
			return n==0 ? true : false;
		}

	public:
		Stack(){
			v=new H*[DIM];
			n=0;
			top=DIM-1;
		}

		int getN() const{
			return n;
		}

		Stack<H>* push(H x){
			if(!_isFull()){
				v[top]=new H(x);
				n++;
				if(top!=0){
					top--;
				}
			}
			else{
				cout<<"Stack Overflow"<<endl;
			}
			return this;
		}

		Stack<H>* pop(){
			if(!_isEmpty()){
				if(top!=DIM-1){
					top++;
				}
				n--;
			}
			else{
				cout<<"Stack Underflow"<<endl;
			}
			return this;
		}

		void print(){
			if(!_isEmpty()){
				for(int i=top+1; i<DIM; i++){
					cout<<*v[i]<<" ";
				}
			}
			cout<<endl;
		}
};


class SDSet : public Stack<SDCode>{

	private:
		void _mergesort(int left, int right){
			if(left==right){
				return;
			}

			int middle=(left+right)/2;
			_mergesort(left, middle);
			_mergesort(middle+1, right);

			SDCode** b=new SDCode*[DIM];
			int i=left; //nuovo array
			int j=left; //prima metà
			int k=middle+1; //seconda metà

			while(j<=middle && k<=right){
				if(*v[j]<*v[k]){
					b[i]=v[j];
					j++;
				}
				else{
					b[i]=v[k];
					k++;
				}
				i++;
			}
			while(j<=middle){
				b[i]=v[j];
				i++;
				j++;
			}
			while(k<=right){
				b[i]=v[k];
				i++;
				k++;
			}

			for(int m=left; m<=right; m++){
				v[m]=b[m];
			}
		}


	public:
		SDSet(){
			Stack<SDCode>();
		}

		void sort(){
			_mergesort(top+1, DIM-1);
		}
};


int main(){

	char cab[11] = "G34TRdfe12";
	int fir=39541;
	SDCode *a=new SDCode(fir, cab);
	SDCode *b=new SDCode(34893, "G34TRxcVF2");
	SDCode *c=new SDCode(34891, "G34TRxcVF2");
	SDCode *d=new SDCode(34891, "G34TRedc11");
	
	if(*a<*b){
		cout<<"true";
	}
	else{
		cout<<"false";
	}

	cout<<endl;

	if(*a>=*b){
		cout<<"true";
	}
	else{
		cout<<"false";
	}

	cout<<endl;

	if(*a==*c){
		cout<<"true";
	}
	else{
		cout<<"false";
	}

	cout<<endl;

	if(*a<=*c){
		cout<<"true";
	}
	else{
		cout<<"false";
	}

	cout<<endl;

	if(*c<*d){
		cout<<"true";
	}
	else{
		cout<<"false";
	}

	cout<<endl;

	if(*d<*c){
		cout<<"true";
	}
	else{
		cout<<"false";
	}

	cout<<endl;

	if(*c!=*d){
		cout<<"true";
	}
	else{
		cout<<"false";
	}

	cout<<endl;

	cout<<*a<<endl;
	cout<<*d<<endl;


	Stack<int>* s=new Stack<int>();
	s->push(2)->push(3)->push(12)->push(4)->push(9)->push(5);
    s->print();  
    cout << endl;
    
    s->pop()->pop()->pop()->pop();
    s->print();  
    cout << endl;
    
    s->push(1)->push(6)->push(23)->push(27);
    s->print();  
    cout << endl;

    SDSet *q = new SDSet();
    q->push(*a)->push(*b)->push(*c)->pop()->push(*d)->push(*c)->print();
    q->sort();
    q->print();
}
