#include <iostream>
using namespace std;

template <class H> class Node{
	private:
			Node <H>* left, * right, * parent;
			int priority; H key;
	public:
			void setLeft( Node <H>* left){ this->left = left;}
			void setRight (Node <H>* right){ this->right = right; }
			void setParent (Node <H>* parent ){ this->parent = parent; }
			void setKey (H key){ this->key = key; }
			void setPriority(int priority) { this->priority = priority; }
			
			Node <H>* getLeft() { return this->left;}
			Node <H>* getRight() { return this->right;}
			Node <H>* getParent() { return this->parent;}
			H getKey() { return this->key;}
			int	getPriority() { return this->priority; }
				
			bool isLeaf() {  return (!getRight() && !getLeft())? true : false; }
			
			Node(int priority,H key) {
				left = right = parent = NULL;
				this->key = key;
				this->priority = priority;
			}
};


template <class H> class PBT {
	public:
			virtual PBT<H>* ins(int p, H x) = 0;
			virtual H find(H x) = 0;
			virtual void print() = 0;
			virtual PBT<H>* del(H x)  = 0;
};

template <class H> class MyPBT: public PBT <H> {
	private:
			Node <H>* root;
			int size;
			
			void _inorder(Node <H>* temp) {
				if(!temp) return;
				_inorder(temp->getLeft());
				cout << temp->getKey() << " ";
				_inorder(temp->getRight());
			}
			
			Node <H>* getMin(Node <H>* temp){
				if(!temp) return NULL;
				while(temp->getLeft() != NULL) temp = temp->getLeft();
				return temp; 
			}
			
			Node <H>* getMax(Node <H>* temp){
				if(!temp) return NULL;
				while(temp->getRight() != NULL) temp = temp->getRight();
				return temp;
			}
			
			Node <H>* getNext(Node <H>* temp){
				if(!temp) return NULL;
				if(temp == getMax(root)) return NULL;
				if(temp->getRight()) return getMin(temp->getRight());
				while(temp->getParent()->getKey() < temp->getKey()) temp  = temp->getParent();
				return temp->getParent();
			}
			
			bool isInsertable(int p,H x) {
				if(!size) return true;
				Node <H>* temp = getMin(root);
				while(temp != NULL) {
					if(temp->getPriority() == p) return false;
					temp = getNext(temp);
				}
				return true;
			}
			
	public:
			
			PBT<H>* ins(int p, H x) {
				if(!isInsertable(p,x)) return this;
				Node <H>* father = NULL;
				Node <H>* temp = root;
				Node <H>* newNode = new Node<H>(p,x);
				while(temp != NULL) {
					father = temp;
					if(temp->getKey() > x) temp = temp->getLeft();
					else temp = temp->getRight();
				}
				
				if(!father) root = newNode;
				else {
					if(father->getKey() > x) father->setLeft(newNode);
					else father->setRight(newNode);
					newNode->setParent(father);
				}
				size++;
				return this;
			}
			
			void inorder(){
				_inorder(root);
				cout << endl;
			}
			
			H find(int p){
				if(!size) return 0;
				Node <H>* temp = getMin(root);
				while(temp != NULL) {
					if(temp->getPriority() == p) return temp->getKey();
					temp = getNext(temp);
				}
				return 0;
			}
			
			int getMaxPriority() {
				if(!size) return -1;
				int priority = 0;
				Node <H>* temp = getMin(root);
				while(temp != NULL) {
					if(priority < temp->getPriority()) priority = temp->getPriority();
					temp = getNext(temp);
				}
				return priority;
			}
			
			int getMinPriority() {
				if(!size) return -1;
				int priority = 0;
				Node <H>* temp = getMin(root);
				while(temp != NULL) {
					if(priority > temp->getPriority()) priority = temp->getPriority();
					temp = getNext(temp);
				}
				return priority;
			}
			
			Node <H>* search(Node <H>* temp,H x) {
				if(!size) return NULL;
				if(!temp) return NULL;
				while(temp != NULL) {
					if(temp->getKey() == x) return temp;
					else if(temp->getKey() >= x) temp = temp->getLeft();
					else temp = temp->getRight(); 
				}
				return NULL;
			}
			void print() {
				int j = getMinPriority();
				for(int i = getMaxPriority(); i >= j;i--) {
					if(find(i)) cout << find(i) << " ";
				}
				cout << endl;
			}
			
			PBT<H>* _del(Node <H>* r, H x) {
				if(!size) return this;
				Node <H>* temp = search(r,x);
				if(!temp) return this;
				
				Node <H>* father = temp->getParent();
				if(temp->isLeaf()) {
					size--;
					if(!father) root = NULL;
					else if(father->getRight() == temp) father->setRight(NULL);
					else father->setLeft(NULL);
					return this;
				}
				
				Node <H>* son = NULL;
				if(!temp->getRight() || !temp->getLeft()) {
					size--;
					if(!temp->getRight()) son = temp->getLeft();
					else son = temp->getRight();
					
					if(!father) root = son;
					else if(father->getRight() == temp) father->setRight(son);
					else father->setLeft(son);
					son->setParent(father);
					return this;
				}
				
				Node <H>* next = getNext(temp);
				temp->setKey(next->getKey());
				_del(temp->getRight(),next->getKey());
				return this;
				
			}
			PBT<H>* del(H x) {return _del(root,x);}
}; 

int main(){
	MyPBT<int>* PriorityTree = new MyPBT<int>();
	PriorityTree->ins(10,5);
	PriorityTree->ins(7,3);
	PriorityTree->ins(8,13);
	PriorityTree->ins(13,1);
	PriorityTree->ins(6,2);
	PriorityTree->ins(9,7);
	PriorityTree->inorder();
	PriorityTree->print();
	PriorityTree->del(13);//->del(1);
	PriorityTree->inorder();
	PriorityTree->print();
}
