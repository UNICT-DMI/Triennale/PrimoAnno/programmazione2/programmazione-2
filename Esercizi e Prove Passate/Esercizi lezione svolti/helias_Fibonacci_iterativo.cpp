/*
  Scrivere un algoritmo che scriva N caratteri 
  della sequenza di Fibonacci in modo iterativo

  Coded By Helias
*/

#include <iostream>
using namespace std;

void fibonacci(int n)
{
	int a[n];
	for(int i = 0; i < n; i++)
	{
		if (i >= 2)
			a[i] = a[i-1] + a[i-2];
		else
			a[i] = i;
		cout << a[i] << " ";
	}
	cout << endl;
}

int main()
{
	int n;
	cout << "Quanti caratteri di fibonacci vuoi generare?" << endl;
	cin >> n;
	fibonacci(n);
	return 0;
}
