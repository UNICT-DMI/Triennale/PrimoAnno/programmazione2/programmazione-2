#include <iostream>
#include <ctime>
#include <cstdlib>
using namespace std;

#define N 10

void carica(int v[],int n){
	for(int i=0;i<n;i++)
		v[i]=rand()%100;
}

void bubblesortPointer(int *v[], int n){
	int* temp;

	for(int i=0;i<n-1;i++)
		for(int k=0;k<n-i-1;k++)
			if(*v[k] > *v[k+1]){
				temp=v[k];
				v[k]=v[k+1];
				v[k+1]=temp;

			}
}

void stampa1(int v[], int n){
	for(int i=0;i<n;i++)
		cout<<*(v+i)<<" ";
	cout<<endl;
}

void stampap(int *v[], int n){
	for(int i=0;i<n;i++)
		cout<<*v[i]<<" "; //**(v+i)
	cout<<endl;
}



int main(){
	int A[N] = {10,2,1,2,41,25,14,12,3,6};
	int *B[N];

	for(int i=0;i<N;i++) B[i]=A+i;

	cout<<"B : ";
	stampap(B,N);
	cout<<"B : ";
	bubblesortPointer(B,N);
	stampap(B,N);
	cout<<"A : ";
	stampa1(A,N);









	return 0;
}