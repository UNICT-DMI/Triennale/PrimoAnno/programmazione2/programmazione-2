#include <iostream>
#include <new>
#include <cstdlib>

using namespace std;

#define N 10

void overflow(){

	cout<<"Memoria insufficiente - fermare l'esecuzione"<<endl;
	cin.get();
	exit(1);
}



int main(){
	set_new_handler(overflow);

	long dimension;
	int *pi;
	int nblocco=1;

	cout<<"Dimensione :";
	cin>>dimension;

	for(nblocco=1; ; nblocco++){
		pi=new int[dimension];
		cout<<"Allocato numero "<<nblocco<<endl;
	}
    
	return 0;
}
