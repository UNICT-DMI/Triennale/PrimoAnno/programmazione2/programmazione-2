#include <iostream>

using namespace std;

#define MAX 100

class Voti{
	public:
		Voti();
		void inserisci(int voto);
		double media_voti();
	private:
		int voti[MAX];
		int n;
};

Voti :: Voti(){
	n=0;
	for(int i=0;i<MAX;i++)
		voti[i]=0;
}

void Voti :: inserisci(int voto){
	voti[n++]=voto;
}

double Voti :: media_voti(){
	int som=0;
	for(int i=0;i<n;i++)
		som+=voti[i];
	return (double)(som/n);
}

int main(){
	Voti voti;
	bool flag=1;
	cout<<"Inserisci i voti (e un numero negativo per terminare)"<<endl;
	do{
		double voto;
		cout<<"--";
		cin>>voto;
		if(voto<0) flag=0;
		else voti.inserisci(voto);
	}while(flag);

	double media = voti.media_voti();

	cout<<"La media e' : "<<media<<endl;
	return 0;
}