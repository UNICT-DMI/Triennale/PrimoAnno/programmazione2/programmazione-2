#include <iostream>

using namespace std;

class Rettangolo{
	public:
		double area(){ return base*altezza; }
		void modifica(double base,double altezza )
		{
			this->base=base;
			this->altezza=altezza;
		}

		void stampa(){
			cout<<"Base : "<<base<<" Altezza : "<<altezza<<endl;
		}

	private:
		double base;
		double altezza;



};

int main(){
	Rettangolo r;
	r.stampa();
	r.modifica(5,7);
	r.stampa();
	cout<<"Area : "<<r.area()<<endl;


	return 0;
}