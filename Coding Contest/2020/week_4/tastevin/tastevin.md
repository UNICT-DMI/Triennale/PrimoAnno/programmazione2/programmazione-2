# Il tastevin avvelenato

Ludovico Grassi Magnani era stato per più di 35 anni un caro amico del padre del Commissario Salvo Montalbano. Di origini toscane, si era trasferito a Fela che era ancora un picciottazzo. Dopo aver preso la laurea in scienze agrarie a Catania si era trasferito a Vigata dove aveva iniziato l'attività di produttore vinicolo in società con il padre del Commissario. Per Salvo Montalbano, Ludovico Grassi Magnani era come uno zio, e da lui aveva imparato quello che oggi conosceva della coltura delle viti e dalla produzione del vino. Quando suo padre venne a mancare, quattro anni avanti, fu Ludovico a dare la triste notizia al Commissario.
Ora Salvo Montalbano taliava incredulo il corpo del Dott. Grassi Magnani stinnicchiato sulla rina, privo di vita. Secondo il Dott. Pasquano, medico della scientifica, non c'erano dubbi, l'uomo era stato avvelenato. Dalle analisi del caso era venuto fuori che Ludovico Grassi Magnani aveva ingerito una tale quantità di veleno in grado di uccidere un bue. Tracce di veleno erano state ritrovate sul tastevin che l'uomo aveva ancora al collo, regalatogli dal padre del Commissario in occasione delle sue nozze.
La scientifica non era però stata in grado di stabilire la quantità esatta del veleno ingerito dalla vittima. In base alle indagini del Commissario, Ludovico Grassi Magnani, poche ore prima di morire aveva preso parte ad una degustazione di vini, presso una casa privata, adibita a cantina, in località Contrada Portotorre. Il sommeliere che si occupava delle degustazioni, in quell'occasione, aveva servito agli avventori numerosi vini pregiati. Se il veleno fosse stato messo nel tastevin della vittima, di certo ne avrà ingerito una piccola quantità ad ogni degustazione. La domanda era quante saranno state. Il Commissario, che conosceva bene l'uomo che fino a quel giorno era stato per lui come un zio, sapeva come ottenere quell'informazione.

## Specifiche
Aiuta il commissario Montalbano ad individuare il numero massimo di vini che Ludovico Grassi Magnani ha degustato durante la serata. Il sommeliere propone agli avventori, in successione, una serie di vini da degustare. Ogni avventore può decidere se assaggiare o meno il vino proposto in quel momento. Il Commissario sa bene che Ludovico, durante le degustazioni, sceglieva i vini in modo che la gradazione alcolica non scendesse mai, quindi, se aveva appena bevuto un vino con gradazione G, successivamente poteva bere solo vini con gradazione almeno pari a G. Inoltre, poiché Ludovico era molto meticoloso negli assaggi, dopo aver degustato un vino puliva meticolosamente il suo tastevin. Questo faceva si che, una volta degustato un vino, fosse costretto a saltare la successiva degustazione poiché impegnato nella pulizia del suo tastevin.

## Dati in input
L'input è costituito da 100 righe, una per ogni task. Ogni riga del file di input contiene un valore N che indica il numero di vini che sono stati serviti nella serata, seguito da N interi positivi i quali rappresentano, in successione, le gradazioni alcoliche dei vini che sono stati serviti nell'ordine proposto dal sommeliere.

Dati in output
Il file di output è composto da 100 righe, una per ogni task presente nel file di input. Ogni riga conterrà il numero massimo di vini che la vittima ha potuto bere durante la serata.

## Note
Il valore di N è sempre compreso tra 4 e 75.
Le gradazioni dei vini esposti sono sempre comprese tra 1 e 100.

### Esempio
Il seguente esempio presenta un file di input, contenente 2 task, ed il corrispondente file di output.

input.txt
9 11 13 10 16 12 12 13 11 13
5 92 51 73 99 48

output.txt
4
2

Spiegazione:
Nel primo caso d'esempio è possibile bere al massimo 4 bicchieri di vino bevendo i vini in posizione 0, 4, 6 e 8.
