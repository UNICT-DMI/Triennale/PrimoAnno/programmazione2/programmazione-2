#include <iostream>
#include <fstream>
using namespace std;

template <class H>
class Nodo{
    protected:
        H elemento;
        Nodo<H>* succ;
    public:
        Nodo(H el): elemento(el){
            this->succ = NULL;
        }

        //Get
        H getElemento(){return elemento;}
        Nodo<H>* getSucc(){return succ;}

        //Set
        void setSucc(Nodo<H>* succ){this->succ = succ;}
};

template <class H>
class Stack{
    protected:
        Nodo<H>* testa;
    public:
        Stack(){testa = NULL;}

        void push(H);
        H pop();  
        bool Pilavuota(){return testa == NULL;}
};

template <class H> void Stack<H>::push(H x){
    Nodo<H>* nuovo = new Nodo<H>(x);

    nuovo->setSucc(testa);
    testa = nuovo;
}

template <class H> H Stack<H>::pop(){
    if(testa != NULL){
        H el = testa->getElemento();
        Nodo<H>* tmp = testa;
        testa = testa->getSucc();
        delete tmp;
        return el;
    }
    return -1;
}

template <class H>
class Coda{
    protected:
        Nodo<H>* testa;
    public: 
        Coda(){this->testa = NULL;}

        void enqueue(H);
        H dequeue();
        void print(ofstream&);
};

template <class H> void Coda<H>::enqueue(H x){
    Nodo<H>* nuovo = new Nodo<H>(x);

    if(testa == NULL)
        testa = nuovo;
    else{
        Nodo<H>* iter = testa;

        while(iter->getSucc() != NULL)
            iter = iter->getSucc();
        
        iter->setSucc(nuovo);
    }
}

template <class H> H Coda<H>::dequeue(){
    if(testa != NULL){
        H el = testa->getElemento();
        Nodo<H>* tmp = testa;
        testa = testa->getSucc();
        delete tmp;
        return el;
    }
    return (H) -1;
}

template <class H> void Coda<H>::print(ofstream& out){
    Nodo<H>* iter = testa;

    while(iter != NULL){
        out << iter->getElemento() << " ";
        iter = iter->getSucc();
    }
    out << endl;
}

template <class H>
class PilaCoda{
    protected:
        Stack<H>* pila;;
        Coda<H>* coda;
    public:
        PilaCoda(){
            pila = new Stack<H>();
            coda = new Coda<H>();
        }

        //Get
        Stack<H>* getPila(){return pila;}
        Coda<H>* getCoda(){return coda;}

        void Travasa();
};

template <class H> void PilaCoda<H>::Travasa(){
    if(!pila->Pilavuota()){
        while(!pila->Pilavuota()){
            coda->enqueue(pila->pop());
        }
    }
}

int getDuepunti(string x){
    for(int i = 0; i < x.length(); i++)
        if(x[i] == ':')
            return i;
    return 0;
}

string getOp(string x){
    return x.substr(0, getDuepunti(x));
}

string getVal(string x){
    return x.substr(getDuepunti(x)+1, x.length());
}

int main(){
    ifstream in("input.txt");
    ofstream out("output.txt");

    for(int i = 0; i < 100; i++){
        string type; in >> type;
        int n; in >> n;

        if(type == "int"){
            PilaCoda<int> t;
            for(int j = 0; j < n; j++){
                string tmp; in >> tmp;
                if(tmp == "pop")
                    t.getPila()->pop();
                else if(tmp == "dequeue")
                    t.getCoda()->dequeue();
                else if(tmp == "travasa")
                    t.Travasa();
                else{
                    string op = getOp(tmp);
                    string _val = getVal(tmp);
                    int val = stoi(_val);

                    if(op == "p")
                        t.getPila()->push(val);
                    else if(op == "e")
                        t.getCoda()->enqueue(val);
                }
            }
            t.getCoda()->print(out);
        }
        else if(type == "bool"){
            PilaCoda<bool> t;
            for(int j = 0; j < n; j++){
                string tmp; in >> tmp;
                if(tmp == "pop")
                    t.getPila()->pop();
                else if(tmp == "dequeue")
                    t.getCoda()->dequeue();
                else if(tmp == "travasa")
                    t.Travasa();
                else{
                    string op = getOp(tmp);
                    string _val = getVal(tmp);
                    bool val = stoi(_val);

                    if(op == "p")
                        t.getPila()->push(val);
                    else if(op == "e")
                        t.getCoda()->enqueue(val);
                }
            }
            t.getCoda()->print(out);
        }

        else if(type == "double"){
            PilaCoda<double> t;
            for(int j = 0; j < n; j++){
                string tmp; in >> tmp;
                if(tmp == "pop")
                    t.getPila()->pop();
                else if(tmp == "dequeue")
                    t.getCoda()->dequeue();
                else if(tmp == "travasa")
                    t.Travasa();
                else{
                    string op = getOp(tmp);
                    string _val = getVal(tmp);
                    double val = stod(_val);

                    if(op == "p")
                        t.getPila()->push(val);
                    else if(op == "e")
                        t.getCoda()->enqueue(val);
                }
            }
            t.getCoda()->print(out);
        }

        else if(type == "char"){
            PilaCoda<char> t;
            for(int j = 0; j < n; j++){
                string tmp; in >> tmp;
                if(tmp == "pop")
                    t.getPila()->pop();
                else if(tmp == "dequeue")
                    t.getCoda()->dequeue();
                else if(tmp == "travasa")
                    t.Travasa();
                else{
                    string op = getOp(tmp);
                    string _val = getVal(tmp);
                    char val = _val[0];

                    if(op == "p")
                        t.getPila()->push(val);
                    else if(op == "e")
                        t.getCoda()->enqueue(val);
                }
            }
            t.getCoda()->print(out);
        }
    }
    


}