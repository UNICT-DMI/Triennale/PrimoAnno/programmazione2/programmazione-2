#include <iostream>
#include <fstream>
using namespace std;

template <class H>
class Nodo{
    private:
        H elemento;
        Nodo<H>* padre;
        Nodo<H>* dx;
        Nodo<H>* sx;
    public:
        Nodo(H _elemento): elemento(_elemento){
            this->padre = NULL;
            this->sx = NULL;
            this->dx = NULL;
        }

        //Get
        H getElemento(){return elemento;}
        Nodo<H>* getPadre(){return padre;}
        Nodo<H>* getDx(){return dx;}
        Nodo<H>* getSx(){return sx;}
        //Set
        void setPadre(Nodo<H>* padre){this->padre = padre;}
        void setDx(Nodo<H>* dx){this->dx = dx;}
        void setSx(Nodo<H>* sx){this->sx = sx;}
        void setElemento(H x){this->elemento =  x;}
};

template <class H>
class BST{
    private:
        Nodo<H>* radice;

        void visitaInorder(Nodo<H>*, H* vett, int &i);   //OK
        void visitaPostorder(Nodo<H>*, ofstream&); //OK
    public:
        BST(){this->radice = NULL;} //OK

        BST<H>* ins(H); //OK
        void print(ofstream&);
        void Naturalfill(H* vett, int &i);
};

template <class H> BST<H>* BST<H>::ins(H x){
    Nodo<H>* nuovo = new Nodo<H>(x);
    if(radice == NULL)
        radice = nuovo;
    else{
        Nodo<H>* iter = radice;
        Nodo<H>* tmp = NULL;
        while(iter != NULL){
            tmp = iter;
            if(x > iter->getElemento())
                iter = iter->getDx();
            else
                iter = iter->getSx();
        }

        nuovo->setPadre(tmp);
        if(x > tmp->getElemento())
            tmp->setDx(nuovo);
        else
            tmp->setSx(nuovo);
    }
    return this;
}

template <class H> void BST<H>::Naturalfill(H* vett, int &i){
    this->visitaInorder(radice, vett, i);
}


template <class H> void BST<H>::visitaInorder(Nodo<H>* ptr, H* vett, int &i){
    if(ptr != NULL){
        visitaInorder(ptr->getSx(), vett, i);
        ptr->setElemento(vett[i]);
        i++;
        visitaInorder(ptr->getDx(), vett, i);
    }
}

template <class H> void BST<H>::visitaPostorder(Nodo<H>* ptr, ofstream &out){
    if(ptr != NULL){
        visitaPostorder(ptr->getSx(), out);  
        visitaPostorder(ptr->getDx(), out);
        out << ptr->getElemento() << " ";
    }
}

template <class H> void BST<H>::print(ofstream &out){
    this->visitaPostorder(radice, out);
    out << endl;
}

int main(){
    
    ifstream in("input.txt");
    ofstream out("output.txt");


    for(int i = 0; i < 100; i++){
        string x; in >> x;
        int n; in >> n;
        if(x == "int"){
            BST<int>* t = new BST<int>();

            for(int j = 0; j < n; j++){
                int aux; in >> aux;
                t->ins(aux);
            }

            int* vett = new int[n];
            for(int j = 0; j < n; j++){
                int aux; in >> aux;
                vett[j] = aux;
            }
            int indice = 0;
            t->Naturalfill(vett, indice);
            t->print(out);

            delete [] vett;
        }

        if(x == "double"){
            BST<double>* t = new BST<double>();

            for(int j = 0; j < n; j++){
                double aux; in >> aux;
                t->ins(aux);
            }

            double* vett = new double[n];
            for(int j = 0; j < n; j++){
                double aux; in >> aux;
                vett[j] = aux;
            }
            int indice = 0;
            t->Naturalfill(vett, indice);
            t->print(out);
            delete [] vett;
        }
    }
}

