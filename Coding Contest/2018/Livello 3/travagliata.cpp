#include <iostream>
#include <fstream>
#include <cmath>
using namespace std;

int travagliata(int* vett, int n){
    int contatore = 0;
    int max = 0;
    int tmp;
    int pos = 0;
    for(int i = 0; i < n; i++){
        for(int j = 0; j < n; j++){
            if(vett[j] > max){
                max = vett[j];
                tmp = j;
            }
        }
        contatore += fabs(tmp - pos);
        vett[tmp] = 0;
        pos = tmp;
        max = 0;
    }
        
    return contatore;
}


int main(){

    ifstream in("input.txt");
    ofstream out("output.txt");

    for(int i = 0; i < 10; i++){
        int n;
        in >> n;
        int* vett = new int[n];
        for(int j = 0; j < n; j++){
            in >> vett[j];
            //cout << vett[j] << " ";
        }
        //cout << endl;
        out << travagliata(vett, n) << endl;
        //cout << travagliata(vett, n) << endl;
    }



    return 0;
}