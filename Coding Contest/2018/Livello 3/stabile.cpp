#include <iostream>
#include <fstream>
using namespace std;

int stabile(int*vett, int n){
    int min;
    int sum = 0;
    for(int i = 0; i < n - 1; i++){
        min = i;
        for(int j = i+1; j < n; j++)
            if(vett[j] < vett[min])
                min = j;
        
        
            if(min != i){
            sum += min - i;
            int tmp = vett[i];
            vett[i] = vett[min];

            for(int j = min - 1; j > i; j--)
                vett[j+1] = vett[j];
            vett[i+1] = tmp;
        }
    }

    return sum;

}


int main(){

    ifstream in("input.txt");
    ofstream out("output.txt");

    for(int i = 0; i < 100; i++){
        int n;
        in >> n;
        int* vett = new int[n];

        for(int j = 0; j < n; j++)
            in >> vett[j];

        out << stabile(vett,n) << endl;
        
        delete [] vett;
    }


    return 0;
}