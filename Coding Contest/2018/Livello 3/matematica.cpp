#include <iostream>
#include <fstream>
using namespace std;


int soluzione(int a, int b){
    int contatore = 0;
    
    
    for(int i = 1; i < 100002; i++){
        if(a == 1 && b == 1)
            return contatore;
        contatore++;
        if(a%2 == 0 && b%2 == 0){
            a = a/2;
            b = b/2;
        }
        else if(a%2 != 0 && b%2 != 0){
            a = a*3 + 1;
            b = b*3 + 1;
        }
        else{
            if(a%2 == 0)
                b += 3;
            else
                a += 3;
        }
    }
    
    return -1;
}


int main(){

    ifstream in("input.txt");
    ofstream out("output.txt");


    for(int i = 0; i < 100; i++){
        int a;
        int b;
        in >> a; in >>b;
        out << soluzione(a,b) << endl;
    }



    return 0;
}