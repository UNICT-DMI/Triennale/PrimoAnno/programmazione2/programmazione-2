#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
using namespace std;


class Carta{
    private:
        int n;
        char seme;
    public:
        Carta(int n, char seme){
            this->n = n;
            this->seme = seme;
        }

    int getN(){return n;}

    char getSeme(){return seme;}

    bool ordseme(Carta &obj){
        if(this->getSeme() > obj.getSeme())
            return true;
        return false;
    }

    bool ordnum(Carta &obj){
        if(this->getSeme() ==  obj.getSeme() && this->getN() < obj.getN())
            return true;
        return false;
    }

};



void soluzione(Carta*vett[], int n, ofstream& out){

    if(n == 1)
        out << vett[0]->getN() << vett[0]->getSeme() << " ";
    else{
        for(int i = 0; i < n; i++)
            for(int j = i + 1; j < n; j++)
                if(vett[i]->ordseme(*vett[j]))
                    swap(vett[i], vett[j]);
        for(int i = 0; i < n; i++)
            for(int j = 0; j < n; j++)
                if(vett[i]->ordnum(*vett[j]))
                    swap(vett[i], vett[j]);


        for(int i = 0; i < n; i++)
            out << vett[i]->getN() << vett[i]->getSeme() << " ";
    }

}



int main(){

    ifstream in("input.txt");
    ofstream out("output.txt");


    for(int i = 0; i < 100; i++){
        int n;
        in >> n;
        Carta* vett[n];
        for(int j = 0; j < n; j++){
            string x;
            in >> x;
            string tmp = x.substr(0, x.length() - 1);
            int n = stoi(tmp);
            
            vett[j] = new Carta(n, x[x.length() - 1]);
        }

        for(int i = 1; i <= n; i++)
            soluzione(vett, i, out);
        out << endl;
    }


    return 0;
}