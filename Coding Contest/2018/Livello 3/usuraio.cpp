#include <iostream>
#include <fstream>
#include <climits>
#include <cmath>
using namespace std;


void ordina(int* &vett, int n){
    for(int i = 0; i < n -1; i++)
        for(int j = i + 1; j < n; j++)
            if(vett[i] > vett[j])
                swap(vett[i], vett[j]);
}

int soluzione(int*vett, int n, int k){
    int diff = INT_MAX;
    
    for(int i = 0; i < n; i++){
        int tmp = 0;
        for(int j = i + k - 1; j > i; j--)
            tmp = tmp + abs(vett[j] - vett[j -1]);
        if(tmp < diff)
            diff = tmp;
    }

    return diff;

}

int main(){

    ifstream in("input.txt");
    ofstream out("output.txt");

    for(int i = 0; i < 100; i++){
        int n;
        int k;
        in >> n;
        in >> k;
        int* vett = new int[n];
        for(int j = 0; j < n; j++)
            in >> vett[j];
        ordina(vett, n);
        out <<  soluzione(vett, n, k) << endl;
    }


    return 0;
}