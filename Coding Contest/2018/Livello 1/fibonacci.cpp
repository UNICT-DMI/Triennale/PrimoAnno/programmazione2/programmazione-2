#include <iostream>
#include <fstream>
using namespace std;

int fibonacci(int n){
    if(n == 0) return 1;
    else if(n == 1) return 1;
    else
        return fibonacci(n-1) + fibonacci(n-2);
}

int main(){
    int a[10];
    
    ifstream in;
    in.open("input.txt");
    for(int i = 0; i < 10; i++)
        in >> a[i];

    in.close();
    cout << endl;
    for(int j = 0; j < 10; j++)
        cout << a[j] << " ";
    cout << endl << endl;

    ofstream file("output.txt");
    for(int i = 0; i < 10; i++)
        cout << fibonacci(a[i] - 1) << " ";

    for(int i = 0; i < 10; i++)
        file << fibonacci(a[i] - 1) << endl;

    for(int i = 0; i < 10; i++)
        cout << fibonacci(a[i] - 1) << " ";
    file.close();


    return 0;
}