#include <iostream>
#include <fstream>
using namespace std;

void soluzione(float*vett, int n, ofstream& out){
    float sum = 0;

    for(int i = 0; i < n; i++)
        sum += vett[i];

    out << sum << endl;
}


int main(){

    ifstream in("input.txt");
    ofstream out("output.txt");

    for(int i = 0; i < 100; i++){
        int n; in >> n;
        float*vett = new float[n];
        for(int j = 0; j < n; j++)
            in >> vett[j];
        
        soluzione(vett, n, out);

        delete [] vett;
    }



    return 0;
}