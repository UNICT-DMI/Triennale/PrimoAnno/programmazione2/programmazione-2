#include <iostream>
#include <fstream>
using namespace std;

int main(){

    ifstream in("input.txt");
    ofstream file("output.txt");

    for(int i = 0; i < 100; i++){
        int n;
        int valore;
        bool v = false;
        in >> n;
        in >> valore;
        int tmp;
        for(int j = 2; j < n + 2; j++){
            in >> tmp;
            if(tmp == valore){
                file << j-1 << endl;
                v = true;
            } 
        }
        if(!v)
            file << 0 << endl;
            
    }

    in.close();
    file.close();

    return 0;
}

