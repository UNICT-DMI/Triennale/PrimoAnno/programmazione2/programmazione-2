#include <iostream>
#include <fstream>
using namespace std;

void soluzione(char*vett, int n, ofstream &out){

    for(int i = 0; i < n; i++){
        if(vett[i] > 'c')
            out << (char)(vett[i] - 3);
        else if(vett[i] == 'a')
            out << (char)'x';
        else if(vett[i] == 'b')
            out << (char)'y';
        else if(vett[i] == 'c')
            out << (char) 'z';
    }

    out << endl;
}

int main(){


    ifstream in("input.txt");
    ofstream out("output.txt");

    for(int i = 0; i < 100; i++){
        int n; in >> n;

        char*vett = new char[n];

        for(int j = 0; j < n; j++)
            in >> vett[j];

        soluzione(vett, n, out);
        delete [] vett;
    }


    return 0;
}