#include <iostream>
#include <fstream>

using namespace std;


template<typename T>
class Node
{
private:
    Node<T> *pLeft, *pRight, *pParent;
    T *pKey;
    size_t nMul;

public:
    Node(const T &key, Node<T> *pParent = NULL)
    {
        pKey = new T(key);
        this->pParent = pParent;
        pLeft = pRight = NULL;
        nMul = 1;
    }

    size_t getMul()
    {
        return nMul;
    }

    void incMul()
    {
        nMul++;
    }

    void decMul()
    {
        nMul--;
    }

    void setKey(const T &key)
    {
        pKey = new T(key);
    }

    T getKey()
    {
        return *pKey;
    }

    void setLeft(Node<T> *pNode)
    {
        pLeft = pNode;
    }

    Node<T>* getLeft()
    {
        return pLeft;
    }

    void setRight(Node<T> *pNode)
    {
        pRight = pNode;
    }

    Node<T>* getRight()
    {
        return pRight;
    }

    Node<T>* getParent()
    {
        return pParent;
    }

    void setParent(Node<T> *pNode)
    {
        pParent = pNode;
    }


};

template <typename T>
class BST
{
private:
    Node<T> *pRoot;

    Node<T>* _insert(T &key, Node<T> *pLastNode, Node<T> *pParent)
    {
        if(pLastNode == NULL)
        {
            pLastNode = new Node<T>(key, pParent);
        }else if(key == pLastNode->getKey())
        {
            pLastNode->incMul();
        }
        else if(key > pLastNode->getKey())
        {
            pLastNode->setRight(_insert(key, pLastNode->getRight(), pLastNode));
        }else
        {
            pLastNode->setLeft(_insert(key, pLastNode->getLeft(), pLastNode));
        }
        return pLastNode;
    }

    Node<T> *_getNext(Node<T> *pLastNode)
    {
        if(pLastNode == NULL)
            return NULL;
        
        if(pLastNode->getRight())
        {
            Node<T> *pTemp = pLastNode->getRight();
            while(pTemp && pTemp->getLeft())
            {
                pTemp = pTemp->getLeft();
            }
            return pTemp;
        }else
        {
            Node<T> *pTemp = pLastNode->getParent();
            while(pTemp && pLastNode->getKey() > pTemp->getKey())
            {
                pTemp = pTemp->getParent();
            }
            return pTemp;
        }
        return NULL;
    }

    Node<T> *_remove(T key, Node<T> *pLastNode)
    {
        if(pLastNode == NULL)
            return NULL;

        if(key == pLastNode->getKey())
        {
            Node<T> *pParent = pLastNode->getParent();
            if(pLastNode->getLeft() == NULL)
            {
                Node<T> *pTemp = pLastNode->getRight();
                if(pTemp)
                    pTemp->setParent(pParent);
                pLastNode = pTemp;
            }else if(pLastNode->getRight() == NULL)
            {
                Node<T> *pTemp = pLastNode->getLeft();
                if(pTemp)
                    pTemp->setParent(pParent);
                pLastNode = pTemp;
            }else
            {
                Node<T> *pNext = _getNext(pLastNode);
                pLastNode->setKey(pNext->getKey());
                pLastNode->setRight(_remove(pNext->getKey(), pLastNode->getRight()));
            }
            

        }else if(key > pLastNode->getKey())
        {
            pLastNode->setRight(_remove(key, pLastNode->getRight()));
        }else
        {
            pLastNode->setLeft(_remove(key, pLastNode->getLeft()));
        }

        return pLastNode;
    }


    void _preOrder(Node<T> *pLastNode, ofstream &ofFile)
    {
        if(pLastNode)
        {
            for (size_t i = 0; i < pLastNode->getMul(); i++)
                ofFile << pLastNode->getKey() << " ";
            
            _preOrder(pLastNode->getLeft(), ofFile);
            _preOrder(pLastNode->getRight(), ofFile);
        }
    }

    void _inOrder(Node<T> *pLastNode, ofstream &ofFile)
    {
        if(pLastNode)
        {
            _inOrder(pLastNode->getLeft(), ofFile);
            for (size_t i = 0; i < pLastNode->getMul(); i++)
                ofFile << pLastNode->getKey() << " ";
            _inOrder(pLastNode->getRight(), ofFile);
        }
    }
    void _postOrder(Node<T> *pLastNode, ofstream &ofFile)
    {
        if(pLastNode)
        {
            _postOrder(pLastNode->getLeft(), ofFile);
            _postOrder(pLastNode->getRight(), ofFile);
            for (size_t i = 0; i < pLastNode->getMul(); i++)
                ofFile << pLastNode->getKey() << " ";
        }
    }

    Node<T> *_search(T &key, Node<T> *pLastNode)
    {
        if(pLastNode == NULL)
            return NULL;

        if(key == pLastNode->getKey())
            return pLastNode;
        
        if(key > pLastNode->getKey())
        {
            return _search(key, pLastNode->getRight());
        }else
        {
            return _search(key, pLastNode->getLeft());
        }
        return NULL;
    }


public:
    BST()
    {
        pRoot = NULL;
    }

    void insert(T key)
    {
        pRoot = _insert(key, pRoot, NULL);
    }

    void remove(T key)
    {
        Node<T> *pTemp = _search(key, pRoot);
        if(pTemp)
        {
            if(pTemp->getMul() > 1)
            {
                pTemp->decMul();
            }else
            {
                pRoot = _remove(key, pRoot);
            }
            
        }
        
    }

    void preOrder(ofstream &ofFile)
    {
        _preOrder(pRoot, ofFile);
    }

    void inOrder(ofstream &ofFile)
    {
        _inOrder(pRoot, ofFile);
    }

    void postOrder(ofstream &ofFile)
    {
        _postOrder(pRoot, ofFile);
    }

};

void jumpTheTrash(ifstream &ifFile)
{
    char trash = '\0';
    while(trash != ':')
        ifFile >> trash;
    
}

template <typename T>
void process(ifstream &ifFile, ofstream &ofFile)
{
    BST<T> *pBST = new BST<T>();
    size_t nOperations;
    string strOpType;
    ifFile >> nOperations;
    ifFile >> strOpType;

    for (size_t i = 0; i < nOperations; i++)
    {
        char chOpType;
        T key;
        ifFile >> chOpType;
        jumpTheTrash(ifFile);
        switch(chOpType)
        {
            case 'i': // ins
            ifFile >> key;
            pBST->insert(key);
            break;
            case 'c':
            ifFile >> key;
            pBST->remove(key);
            break;
        }
    }

    if(strOpType == "preorder")
    {
        pBST->preOrder(ofFile);
    }else if(strOpType == "inorder")
    {
        pBST->inOrder(ofFile);
    }else
    {
        pBST->postOrder(ofFile);
    }
    ofFile << endl;
    
}

int main()
{
    ifstream ifFile("input.txt");
    ofstream ofFile("output.txt");

    string strType;

    while(ifFile >> strType)
    {
        switch(strType[0])
        {
            case 'i':
            process<int>(ifFile, ofFile);
            break;
            case 'd':
            process<double>(ifFile, ofFile);
            break;
            case 'c':
            process<char>(ifFile, ofFile);
            break;
            case 'b':
            process<bool>(ifFile, ofFile);
            break;
        }
    }

    return 0;
}