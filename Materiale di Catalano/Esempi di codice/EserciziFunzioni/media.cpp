/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sulle funzioni

Esercizio 0: Calcolare la media di due reali
*/

#include<iostream>
using namespace std;

double media(double x1, double x2)
{ 
 return (x1+x2)/2; 
}

int main()
{
 double x1,x2,med;
 
 cout << "Introdurre due reali: "; 
 cin >> x1 >> x2; 
 med=media(x1,x2);
 cout << "Il valore medio è " << med << endl;
 return 0; 
}