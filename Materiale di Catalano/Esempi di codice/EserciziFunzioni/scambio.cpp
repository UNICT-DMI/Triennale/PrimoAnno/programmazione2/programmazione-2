/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sulle funzioni

Esercizio 1: Scambiare i valori di due variabili intere
(esempio di passaggio di parametri per valore, per indirizzo e per riferimento)
*/
#include<iostream>
using namespace std;

//void scambia(int, int);
//void scambia_Puntatori(int*, int*); 
//void scambia_Riferimenti(int&, int&);

void scambia(int a, int b)
//Passaggio di argomenti per valore
{
  int aux=a;
  a=b; b=aux; 
}

void scambia_Puntatori(int* a, int* b)
{
  int aux=*a;
  *a=*b; *b=aux; 
}

void scambia_Riferimenti(int& a, int& b)
{
  int aux=a;
  a=b; b=aux; 
}


int main()
{
 int a=10,b=20;
 
 cout << "Valori iniziali a= " << a << ", b= " << b << endl; 
 scambia(a,b);
 cout << "Dopo scambia (per valore) a= " << a << ", b= " << b << endl;
 a=10; b=20;
 cout << "Ri-inizializzo a= " << a << ", b= " <<b << endl;
 scambia_Puntatori(&a, &b);
 cout << "Dopo scambia (con puntatori) a= " << a << ", b= " <<b << endl;
 a=10; b=20;
 cout << "Ri-inizializzo a= " << a << ", b= " <<b << endl;
 scambia_Riferimenti(a,b);
 cout << "Dopo scambia (per riferimento) a= " << a << ", b= " <<b << endl;
 return 0; 
}