/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sui templates

Esercizio 4: Esempio di compilazione per inclusione.
*/

template <class T> int confrontare (T a, T b)
{
	if (a<b) return -1; 
	else if (b<a) return 1;
	else return 0;
}