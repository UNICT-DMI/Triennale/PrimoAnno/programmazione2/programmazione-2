/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi su ordinamento

Esercizio 6: Implementazione del quicksort

*/

#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

template <class H> void swap(H *v, int x, int y) {  
	H tmp = v[x];
	v[x] = v[y];
	v[y] = tmp; 
}   

template <class H> void print (H *v, int n) {
	for(int i = 0; i < n; i++) 	
		cout << v[i] << " ";
	cout << endl;  
}

template <class H> void quickSort(H *v, int start, int end){
	if(start < end){
		int i = start,
			j = end-1,
			mid = end+start/2,
			pivot = v[mid];

		swap(v, mid, end);		
		while(i < j){
			while((v[i] <= pivot) && (i < end)) i++;
			while((v[j] > pivot) && (j >= start)) j--;
			if(i < j) swap(v, i, j);  
		}
		swap(v, i, end);
		
		quickSort(v,start,j);
		quickSort(v,j+1,end);
	} 
}

template <class H> void _quickSort(H *v, int n) //Ordina a[0:n-1]
{
	quickSort(v, 0, n-1); 
}

int main(){	
	srand(time(0));

	int n;
	cout << "Inserisci la dimensione: ";
	cin >> n;

	int *v = new int[n];
	for(int i = 0; i < n; i++){
		v[i] = rand()%n;
	}
	cout << "Vettore iniziale \n"; 
	print(v, n);
	clock_t start, end;
	start = clock();
	_quickSort(v, n);
	end = clock(); 
	cout << endl << "Vettore ordinato " << endl;
	print(v, n); 

	cout << "\nTempo di esecuzione: " << (end-start)*1000 /(double) CLOCKS_PER_SEC << "ms" << endl; 
	

}
