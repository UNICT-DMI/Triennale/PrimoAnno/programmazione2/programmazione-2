/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sugli array

Esercizio: Concatenazione di array. 
Il programma prende due array di 10 elementi ciascuno e 
restituisce in output un array che contiene 20 elementi. 
*/
#include<iostream>
using namespace std;
#define DIM 10

void inserire(int a[],int n)
{
	for(int i=0;i<n;i++)
	{	
		cout << "Inserire un intero: "; 
		cin >> a[i];
	}
}

void stampa(int a[],int n) 
{	
	for(int i=0;i<n;i++)
	{	
		cout << a[i] <<" "; 
	}
	cout << endl; 
}

int* concatena(int a[], int b[], int n)
{	
	// int c[2*n]; 		//Con questo attivo no
	int  *c= new int;   //Con questo attivo funziona
	
	for(int i=0; i<n;i++) 
	{	c[i]=a[i];
		c[i+n]=b[i];
	}
	cout << "Vettore ottenuto dopo la concatenazione:"; 
	stampa(c,2*DIM);
	return c;
}

int main()
{
	int a[DIM], b[DIM];
	//int c[2*DIM]; Questo darebbe errore in fase di compilazione
	int* c; 
	
	cout << "Creazione del vettore a." << endl; 
	inserire(a,DIM);
	cout << "Vettore a:"; 
	stampa(a,DIM);
	cout << "Creazione del vettore b." << endl; 
	inserire(b,DIM);
	cout << "Vettore b:"; 
	stampa(b,DIM);
	c=concatena(a,b,DIM);
	cout << "Vettore ottenuto dopo la concatenazione:"; 
	stampa(c,2*DIM);
}
		  
