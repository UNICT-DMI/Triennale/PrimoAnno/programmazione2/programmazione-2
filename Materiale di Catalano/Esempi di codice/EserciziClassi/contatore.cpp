/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sulle classi

Esercizio 3: Definizione di una classe contatore. 
Usando un costruttore.
*/
#include<iostream>
using namespace std;

class contatore {
	public: 
		contatore(){ conto=1;} // costruttore 
		void incrementa() {conto++;} 
		int leggi_conto() {return conto;} 		
	private: 
		unsigned int conto;
		// Usiamo unsigned perché il conto è sempre positivo
}; 
    
int main()
{
	contatore c1,c2; 
	cout << "c1=" << c1.leggi_conto() << endl; 
	cout << "c2=" << c2.leggi_conto() << endl; 
	c1.incrementa(); 
	c1.incrementa(); 
	c2.incrementa(); 
	cout << "c1=" << c1.leggi_conto() << endl; 
	cout << "c2=" << c2.leggi_conto() << endl; 
}