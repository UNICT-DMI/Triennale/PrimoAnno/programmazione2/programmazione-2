/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sui puntatori

Esercizio 4: Puntatori a funzioni.

*/
#include<iostream>
using namespace std;

double inversi(int k)
{   return 1.0/k; }

double quadrati(int k)
{	return (double) k*k;   }

double somma(int n, double (*f) (int k)) 
{
	double s=0;
	
	for (int i=1; i<=n;i++) s += f(i); 
	return s; 
}

int main()
{ 
	cout << "Somma degli inversi dei primi cinque interi " << somma(5,inversi) << endl; 
	cout << "Somma dei quadrati dei primi 2 interi " << somma(2,quadrati) << endl;  
	return 0; 
}