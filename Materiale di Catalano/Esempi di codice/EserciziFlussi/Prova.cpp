/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi su Flussi e Files

Esercizio 6: Esercizio riportato nel lucido 19.
Stampa a video del contenuto di un file. 
*/
#include<fstream>
#include<iostream>

using namespace std;

int main(int argc, char* argv[])
{
	char c;
   	if (argc != 2) {
    	 cout << "errore nel numero di files\n";
     	return 1;
   		}
   	ifstream ent(argv[1], ios::in | ios::binary);
   	if (!ent)
   	{
    	 cout << "Non si può aprire il file" << endl;
     	return 1;
   	}
   	while (ent)      // ent è 0 quando si raggiunge eof
   	{
    	 ent.get(c);
     	cout << c;
  	 }
   	ent.close();
	return 0;	
}