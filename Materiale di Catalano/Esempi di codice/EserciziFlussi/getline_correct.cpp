/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi su Flussi e Files

Esercizio 2: Problemi con l'uso di getline. 
Versione corretta dell'esercizio precedente. 
*/
#include<iostream>

using namespace std;

int main()
{
	char Nome[30], aux[2]; 
	int eta; 	
	
	cout << "Introdurre l'età: "; 
	cin >> eta; 
	cout << eta << endl;
	cin.getline(aux,2);
	cout << "Introdurre il nome: ";  
	cin.getline(Nome,30); 
	cout << Nome << endl; 
	return 0;	
}