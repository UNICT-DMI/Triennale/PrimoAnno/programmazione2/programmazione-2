/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi su Flussi e Files

Esercizio 5: Copia il contenuto di un file in un altro file, facendo piccole 
modifiche.  
*/
#include<fstream>
#include<iostream>

using namespace std;

int main()
{
	char Nome[30]; 
	
	cout << "Inserire il nome del file. "; 
	cin >> Nome;
	ifstream FileIn(Nome);
	if (!FileIn) 
	{	cerr << "Impossibile aprire il file " << Nome << endl;
		return -1; 
	}
	ofstream Dest("SenzaA"); 
	char c; 
	while(FileIn.get(c)) {
		if ((c!='a') && (c!='A'))
			Dest << c;
		} 
	return 0;	
}